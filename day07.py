from collections import Counter

card_value = lambda ch: '0123456789TJQKA'.find(ch)
card_value_pt2 = lambda ch: 'J123456789T0QKA'.find(ch)

def handranker(handstr, pt=1):
    '''
    Ranks each hand uniquely as a decimal

    Base-10 value of each hand:
       #    ##   ##   ##  ##  ##
       |    |    |    |   |   |
       |    |    |    |   |  Lowest significance, would only appear in "high card" case
       |    |    |    |  Mid-low significance
       |    |    |   Mid significance
       |    |   Next highest, or zero when non-existent (same values as Highest card)
       |   Highest card. Always populated. 2 = 2, 3 = 3 ... T = 10, J = 11 ... A = 14
      Hand type: 7 = 5oaK, 6 = 4oaK, 5 = house ... 1 = high

    Highest possible: 71400000000 (5 Aces)
    Lowest possible:  10605040302 (6-5-4-3-2)
    '''

    hand_cnts = Counter(handstr)
    ordered_card_cnts = sorted(hand_cnts.most_common(), key=lambda a: a[1]*100 + card_value(a[0]), reverse=True)

    if pt == 2:
        # (J => Joker)
        # JJJJJ -> 5oaK, JJJJx -> 5oaK,  JJJxx -> 5oaK, JJJxy -> 4oaK
        # JJxxx -> 5oaK, JJxxy -> 4oaK,  JJxyz -> 3oaK, Jxxxx -> 5oaK
        # Jxxxy -> 4oaK, Jxxyy -> house, Jxxyz -> 3oaK, Jwxyz -> pair
        n_jokers = hand_cnts['J']
        del hand_cnts['J']

        match n_jokers:
            case 5 | 4:
                HV = 70000000000
            case 3:
                HV = 70000000000 if hand_cnts.most_common()[0][1] == 2 else 60000000000
            case 2:
                if hand_cnts.most_common()[0][1] == 3:
                    HV = 70000000000
                elif hand_cnts.most_common()[0][1] == 2:
                    HV = 60000000000
                else:
                    HV = 40000000000
            case 1:
                if hand_cnts.most_common()[0][1] == 4:
                    HV = 70000000000
                elif hand_cnts.most_common()[0][1] == 3:
                    HV = 60000000000
                elif hand_cnts.most_common()[0][1] == 1:
                    HV = 20000000000
                elif hand_cnts.most_common()[1][1] == 2:
                    HV = 50000000000
                else:
                    HV = 40000000000
            case 0:
                pass

    if pt == 1 or n_jokers == 0:
        match len(hand_cnts):
            case 5:  # High card
                HV = 10000000000
            case 4:  # One pair
                HV = 20000000000
            case 3:  # Three of a kind or two pair
                HV = 40000000000 if ordered_card_cnts[1][1] == 1 else 30000000000
            case 2:  # Four of a kind or full house
                HV = 60000000000 if ordered_card_cnts[1][1] == 1 else 50000000000
            case 1:  # Five of a kind
                HV = 70000000000

    for i, card in enumerate(handstr):
        dig_idx = 8 - 2*i
        HV += (card_value(card) if pt == 1 else card_value_pt2(card)) * 10**dig_idx

    return HV

def handstr_sorter(handstr_val_pair, pt=2):
    handstr, _ = handstr_val_pair
    return handranker(handstr, pt=pt)


try:
    infile = open('input07.txt', 'r')
except:
    infile = open('sample07.txt', 'r')

lines = [line[:-1].split(' ') for line in infile.readlines()]
infile.close()
highest_rank = len(lines)

handstr_val_pair = [[line[0], int(line[1])] for line in lines]

handstr_val_pair.sort(key=lambda hs:handstr_sorter(hs, pt=1))
part_one = sum([r*val for r, (_, val) in enumerate(handstr_val_pair, 1)])
print(f'Part 1: {part_one}')

handstr_val_pair.sort(key=lambda hs:handstr_sorter(hs, pt=2))
part_two = sum([r*val for r, (_, val) in enumerate(handstr_val_pair, 1)])
print(f'Part 2: {part_two}')

