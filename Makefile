TARGETS = day01 day05 day08 day09 day12 day15

CC ?= gcc
CXX ?= g++


.PHONY: $(TARGETS)
all: help

day01:
	rm -f day01
	$(CC) -o day01 day01.c

day02:
	@echo "python day02.py"

day03:
	@echo "python day03.py"

day04:
	@echo "python day04.py"

day05:
	rm -f day05
	$(CXX) -o day05 day05.cc

day06:
	@cat day06.txt

day07:
	@echo "python day07.py"

day08:
	rm -f day08
	$(CC) -o day08 day08.c

day09:
	rm -f day09
	$(CXX) -o day09 day09.cc

day10:
	@echo "./day10.js"
	@echo "node day10.js"

day11:
	@echo "python day11.py"

day12:
	rm -f day12
	$(CC) -o day12 day12.c

day13:
	@echo "Not yet implemented!"

day14:
	@echo "Not yet implemented!"

day15:
	rm -f day15
	$(CC) -o day15 day15.c

day16:
	@echo "Not yet implemented!"

day17:
	@echo "Not yet implemented!"

day18:
	@echo "Not yet implemented!"

day19:
	@echo "Not yet implemented!"

day20:
	@echo "Not yet implemented!"

day21:
	@echo "Not yet implemented!"

day22:
	@echo "Not yet implemented!"

day23:
	@echo "Not yet implemented!"

day24:
	@echo "Not yet implemented!"

day25:
	@echo "Not yet implemented!"

clean:
	rm -f $(TARGETS)

help:
	@echo "make day##"
	@echo "  (compilable targets: $(TARGETS))"
