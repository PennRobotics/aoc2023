FILENAME = 'input11.txt'
try:
    infile = open(FILENAME, 'r')
    infile.close()
except:
    FILENAME = 'sample11.txt'

# First pass: find empty rows and columns
rows = set([])
cols = None

infile = open(FILENAME, 'r')
for row, line in enumerate([_[:-1] for _ in infile]):
    if cols is None:
        cols = set(range(len(line)))
    for col, ch in enumerate(line):
        if ch == '#':
            rows |= set([row])
            cols -= set([col])
infile.close()

rows = set(range(1 + row)) - rows

for cur_pass in [2, 3]:
    # Second pass: assign galaxies
    # Third pass: assign galaxies with huge distance
    galaxies = []
    cg = 1
    cr = 0

    infile = open(FILENAME, 'r')
    for row, line in enumerate([_[:-1] for _ in infile]):
        if set([row]) & rows:
            cr += 2 if cur_pass == 2 else (1000000) if FILENAME != 'sample11.txt' else (100)
            continue
        cc = 0
        for col, ch in enumerate(line):
            if set([col]) & cols:
                cc += 2 if cur_pass == 2 else (1000000) if FILENAME != 'sample11.txt' else (100)
                continue
            if ch == '#':
                galaxies.append((cg, cr, cc))
                cg += 1
            cc += 1
        cr += 1
    infile.close()

    # Get Manhattan distance between each galaxy
    d = 0
    for i, (g1, r1, c1) in enumerate(galaxies):
        for g2, r2, c2 in galaxies[i+1:]:
            d += abs(r2-r1) + abs(c2-c1)

    print(f'Part {1 if cur_pass == 2 else 2}: {d}')

