#!/usr/bin/env node
'use strict';

const DEBUG = function(s)  { if (0) console.log(s); }
const UNKNOWN = 0;
const MAZEPIECE = 1;
const CONTAINED = 2;

const fs = require('node:fs');

try {
  var maze = fs.readFileSync('input10.txt', 'utf8').toString().trim().split('\n');
} catch (e) {
  //var maze = fs.readFileSync('sample10.txt', 'utf8').toString().trim().split('\n');
  //var maze = fs.readFileSync('sample10b.txt', 'utf8').toString().trim().split('\n');
  var maze = fs.readFileSync('sample10c.txt', 'utf8').toString().trim().split('\n');
}

DEBUG(maze.join('\n'));
DEBUG('-----');

/* Get maze properties */
const dims = [maze.length, maze[0].length];
const sRow = maze.findIndex(A=>A.includes('S'));
const sCol = maze[sRow].indexOf('S');

const visited = Array.from({length:dims[0]}, () => Array.from({length:dims[1]}, () => UNKNOWN));
visited[sRow][sCol] = MAZEPIECE;

DEBUG(visited.map(A=>A.join('')).join('\n'));
DEBUG('-----');

/* Set up maze follower objects */
let pathTracker = [[-1, -1], [-1, -1], 1];  /* [rNow, cNow], [rPrev, cPrev], dist */
let pathTrackers = [pathTracker, structuredClone(pathTracker)];  /* 1st follower, 2nd follower */

const connectsUp    = '7|F';
const connectsRight = 'J-7';
const connectsDown  = 'L|J';
const connectsLeft  = 'F-L';

let startGoesURDL = [false, false, false, false];
var idx = 0;
if (sRow > 0) {
  if (connectsUp.indexOf(maze[sRow-1][sCol]) > -1) {
    pathTrackers[idx][0] = [sRow-1, sCol];
    pathTrackers[idx][1] = [sRow, sCol];
    idx++;
    startGoesURDL[0] = true;
  }
}
if (sCol+1 < dims[1]) {
  if (connectsRight.indexOf(maze[sRow][sCol+1]) > -1) {
    pathTrackers[idx][0] = [sRow, sCol+1];
    pathTrackers[idx][1] = [sRow, sCol];
    idx++;
    startGoesURDL[1] = true;
  }
}
if (sRow+1 < dims[0]) {
  if (connectsDown.indexOf(maze[sRow+1][sCol]) > -1) {
    pathTrackers[idx][0] = [sRow+1, sCol];
    pathTrackers[idx][1] = [sRow, sCol];
    idx++;
    startGoesURDL[2] = true;
  }
}
if (sCol > 0) {
  if (connectsLeft.indexOf(maze[sRow][sCol-1]) > -1) {
    pathTrackers[idx][0] = [sRow, sCol-1];
    pathTrackers[idx][1] = [sRow, sCol];
    idx++;
    startGoesURDL[3] = true;
  }
}

/* The start character needs to be replaced, because it's part of the "point in polygon" method */
function setCharAt(str, idx, ch) {
  return str.substring(0, idx) + ch + str.substring(idx + 1);
}

maze[sRow] = setCharAt(maze[sRow], sCol, (startGoesURDL[0] && startGoesURDL[1]) ? 'L' :
                                         (startGoesURDL[0] && startGoesURDL[2]) ? '|' :
                                         (startGoesURDL[0] && startGoesURDL[3]) ? 'J' :
                                         (startGoesURDL[1] && startGoesURDL[2]) ? 'F' :
                                         (startGoesURDL[1] && startGoesURDL[3]) ? '-' :
                                         (startGoesURDL[2] && startGoesURDL[3]) ? '7' : die);

/* char, [coordIdx (0=row, 1=col), adjustment], [coordIdx, adjustment] */
const connectors = [['|', [0, -1], [0, +1]],
                    ['7', [0, +1], [1, -1]],
                    ['J', [0, -1], [1, -1]],
                    ['L', [0, -1], [1, +1]],
                    ['F', [0, +1], [1, +1]],
                    ['-', [1, -1], [1, +1]]];

/* Advance both paths one-by-one until they meet */
while (pathTrackers[0][0][0] != pathTrackers[1][0][0] ||
       pathTrackers[0][0][1] != pathTrackers[1][0][1])
{
  for (let i = 0; i < 2; ++i) {
    /* Set up vars that will advance each position of both paths */
    pathTracker = pathTrackers[i];
    let [r, c] = pathTracker[0];
    let ch = maze[r][c];
    let coordPrev = pathTracker[1];
    let coordNext;

    for (let chIdx = 0; chIdx < connectors.length; ++chIdx) {
      /* Find the current character in our lookup table */
      if (ch != connectors[chIdx][0])  { continue; }

      /* Check which direction was previous and which will be next */
      for (let conIdx = 1; conIdx <= 2; ++conIdx) {
        let adjPair = connectors[chIdx][conIdx];
        coordNext = [r, c];
        coordNext[adjPair[0]] += adjPair[1];
        if (coordNext[0] != coordPrev[0] || coordNext[1] != coordPrev[1])  { break; }
      }

      /* Move one position and update current maze follower variables */
      pathTracker[0] = coordNext;
      pathTracker[1] = [r, c];
      pathTracker[2]++;
      pathTrackers[i] = pathTracker;
      visited[r][c] = MAZEPIECE;
    }
  }
}

/* The last maze segment also needs to be marked as visited */
let [r, c] = pathTrackers[0][0];
visited[r][c] = MAZEPIECE;

DEBUG(pathTrackers);
DEBUG('=====');

DEBUG(visited.map(A=>A.join('')).join('\n'));
DEBUG('-----');

/* Check if an unknown cell is interior or exterior using "point in polygon" method */
let contained = 0;
for (let r = 0; r < dims[0]; ++r) {
  let containedIfOne = 0;
  for (let c = 0; c < dims[1]; ++c) {
    if (visited[r][c] == MAZEPIECE && connectsDown.indexOf(maze[r][c]) > -1) {
      containedIfOne ^= 1;  /* toggle */
    } else if (visited[r][c] == UNKNOWN && containedIfOne == 1) {
      contained++;
      visited[r][c] = CONTAINED;
    }
  }
}
DEBUG(visited.map(A=>A.join('')).join('\n'));
DEBUG('-----');

console.log('Part 1: ' + pathTrackers[0][2]);
console.log('Part 2: ' + contained);
