#!/bin/sh

if [ "$1x" = "x" ] || [ "$2x" != "x" ];
then
	echo "Usage:"
	echo "$0 <int>"
	exit 1
fi

day=$(expr $1 + 0) || exit 2  # Removes leading zeroes
dd=$(printf "%02d" "$day")
if [ $day -lt 1 ] || [ $day -gt 25 ];
then
	echo "The script argument should be the number of the requested day, from 1 to 25"
	exit 3
fi

if [ -e input$dd.txt ];
then
	echo "input$dd.txt already exists"
	exit 0
fi

if [ -e cookie.txt ];
then
	echo Retrieving saved cookie
	cookie=`cat cookie.txt`
else
	echo Paste the session cookie
	read -s cookie
	echo "$cookie" > cookie.txt
fi

if [ ${#cookie} -eq 128 ];
then
	echo "Could be a valid cookie. Attempting to retrieve input##"
else
	echo "Bad cookie."
	rm -f cookie.txt
	exit 4
fi

curl -b session=$cookie https://adventofcode.com/2023/day/$day/input -o input$dd.txt && echo "File downloaded. Check contents" || echo "Failed"
