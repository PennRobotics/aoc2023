try:
    infile = open('input04.txt', 'r')
except:
    infile = open('sample04.txt', 'r')

parser = lambda s: set([int(s[i:i+2]) for i in range(0, len(s), 3)])

score = 0
for line in infile:
    _, entries = line[:-1].split(': ')
    winners, nums_to_check = entries.split(' | ')
    pwr = len(parser(nums_to_check) & parser(winners))
    score += 2**(pwr-1) if pwr else 0
print(f'Part 1: {score}')

