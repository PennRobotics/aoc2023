#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t getHash(char *, size_t *, int *);

int main() {
	int result;
	FILE *fp;
	char *inputString = NULL;
	size_t len;

	int part = 1;

	while (part <= 2) {
		result = 0;

		fp = fopen("input15.txt", "r");
		if (fp == NULL)  { fp = fopen("sample15.txt", "r"); }

		getline(&inputString, &len, fp);
		char *c = &inputString[0];

		int endFound = 0;
		while (*c) {
			result += getHash(c, &len, &endFound);
			if (endFound)  { break; }
			c += len;
		}

		printf("Result for Part %d: %d\n", part, result);

		fclose(fp);
		part++;
	}

	free(inputString);
}


inline uint8_t getHash(char *s, size_t *len, int *isEnd)
{
	*len = 1;
	register uint8_t val = 0;

	while ((*s != '\n') && (*s != ',')) {
		val += *s++;
		val *= 17;
		(*len)++;
	}

	*isEnd = (*s == '\n') ? 1 : 0;
	return val;
}
