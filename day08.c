#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define  LEN_ROUTE     269  // TODO: dynamic
#define  INPUT_LENGTH  730  // TODO: dynamic
/// #define  LEN_ROUTE     3  // TODO: dynamic
/// #define  INPUT_LENGTH  3  // TODO: dynamic
#define  TARGET_ID     17575

typedef struct Node {
	uint16_t id;  // d2 * 26^2 + d1 * 26 + d0; AAA = 0 ... AAZ = 25, ABA = 26 ... ZZZ = 17575
	struct Node *left;
	struct Node *right;
} Node;


int main() {
	FILE *fh = fopen("input08.txt", "r");
	/// FILE *fh = fopen("sample08.txt", "r");
	// TODO: open sample if input08.txt not found

	// Get directions list
	char routeStr[LEN_ROUTE];
	fscanf(fh, "%s", routeStr);
	fscanf(fh, "\n");

	Node *setupArray[26*26*26] = {0};  // Better but more wasteful: sparse array, fast look-up time
	uint16_t links[INPUT_LENGTH][3] = {0};

	// I'm expecting Part 2 to need to find cycles and use these to speed up
	//    computation, although starting/ending at a different point would
	//    also be unsurprising.

	char d2, d1, d0;
	char *tmpStr = (char *)malloc(14 * sizeof(char));  // re-used later
	Node *tmp;
	uint16_t id;
	for (int i = 0; i < INPUT_LENGTH; ++i) {
		// Get input line and hash of first node
		fscanf(fh, "%c%c%c = (%[^\n]\n", &d2, &d1, &d0, tmpStr);
		d2 -= 0x41;  d1 -= 0x41;  d0 -= 0x41;

		// Create a Node, assign its id, set it aside for now
		tmp = (Node *)malloc(sizeof(Node));
		id = 676*d2 + 26*d1 + d0;
		tmp->id = id;
		setupArray[id] = tmp;
		links[i][0] = id;

		// Get hash of left link
		sscanf(tmpStr, "%c%c%c, %s", &d2, &d1, &d0, tmpStr);
		d2 -= 0x41;  d1 -= 0x41;  d0 -= 0x41;
		links[i][1] = 676*d2 + 26*d1 + d0;

		// Get hash of right link
		sscanf(tmpStr, "%c%c%c)\n", &d2, &d1, &d0);
		d2 -= 0x41;  d1 -= 0x41;  d0 -= 0x41;
		links[i][2] = 676*d2 + 26*d1 + d0;
	}

	// Find each node using its hash and assign its left and right links using their hashes
	for (int i = 0; i < INPUT_LENGTH; ++i) {
		setupArray[links[i][0]]->left  = setupArray[links[i][1]];
		setupArray[links[i][0]]->right = setupArray[links[i][2]];
	}

	Node *cur = setupArray[0];

	int count = 0;
	int idx = 0;
	while (1) {
		cur = (routeStr[idx] == 'L') ? cur->left : cur->right;
		idx = (idx + 1) % LEN_ROUTE;
		count++;

		if (cur->id == TARGET_ID)  { break; }
	}

	printf("Step 1: %d\n", count);

	// TODO-never: Go through and free everything malloc'd

	return EXIT_SUCCESS;
}
