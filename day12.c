#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define  MAX_COLS  20


inline void fillCurrentRecs(int k, int n)  // k-th iteration out of n
{
	for (bit = 0; bit < n; ++bit) {
		int val = (k>>bit) & 0x1;
		// TODO
}

int main() {
	FILE *fh = fopen("sample12.txt", "r");

	uint8_t currentVals[MAX_COLS]      = {0};
	uint8_t toggleIdxs [MAX_COLS]      = {0};
	uint8_t currentRecs[MAX_COLS >> 1] = {0};  // can have 10 records w/ 20 cells: #.#.#.#. is 8 cells, 4 recs
	uint8_t givenRecs  [MAX_COLS >> 1] = {0};

	int i = 0;  // for filling `currentVals`
	int j = 0;  // for filling `toggleIdxs`
	int k;  // for filling `givenRecs`
	int numChecks = 1;  // Each toggle doubles this
	while (1) {
		char ch = fgetc(fh);  // Ex -> ".??..??...?##. 1,1,3\n"
		if (ch == EOF)  { break; }
		switch (ch) {
			case '.':  currentVals[i++] = 0; break;
			case '#':  currentVals[i++] = 1; break;
			case '?':  toggleIdxs[j++] = i++; numChecks *= 2; break;

			case ' ':  // Save everything everything after this for comparing each iteration
				{
					char ch2 = ' ';
					k = 0;
					while (ch2 != '\n' && ch2 != EOF) {
						int num;
						fscanf(fh, "%d", &num);
						givenRecs[k++] = num;
						ch2 = fgetc(fh);
					}

					// Brute-force all possible records
					for (int chk = 0; chk < numChecks; ++chk) {
						NULL;
					}

					// Next line
					i = 0;
					j = 0;
					numChecks = 1;
					memset(currentVals, 0,  MAX_COLS       * sizeof(uint8_t));
					memset(toggleIdxs,  0,  MAX_COLS       * sizeof(uint8_t));
					memset(currentRecs, 0, (MAX_COLS >> 1) * sizeof(uint8_t));
					memset(givenRecs,   0, (MAX_COLS >> 1) * sizeof(uint8_t));
				}
				break;

			default:
				printf("Unrecognized character %c (%d)\n", ch, (int)ch);  // TODO-debug
				return -1;
		}
	}

	fseek(fh, 0, SEEK_SET);
	fclose(fh);

	printf("Part 1: %d\n", 0);

	return EXIT_SUCCESS;
}
