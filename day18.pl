#!/usr/bin/env perl

# TODO:
# Make a maze that adheres to the Day 12 schema, then call Day 12 for solving

use strict;
use warnings;

my $filename = "sample18.txt";
open my $file, "<", $filename or die "Can't open '$filename' for reading: $^E\n";

while (<$file>) {
	my @lines = split(/\n/, $_);
	foreach (@lines) {
		my ($dir, $dist, $rest) = split(/ /, $_, 3);
		print("line: $dir and $dist\n")  # TODO-debug
	}
}

# TODO: determine bounds of output file and starting point

# TODO: open text file for output

# TODO: create blank array

# TODO: loop through each instruction and fill the array

# TODO: write the array to the output file

# TODO: run the Day 12 script on the generated file

