try:
    infile = open('input03.txt', 'r')
except:
    infile = open('sample03.txt', 'r')

nums_w_bounds = []
symbol_coords = []
gear_coords = []

for r, line in enumerate(infile):
    num = 0
    for c, ch in enumerate(line[:-1]):
        if ch in '0123456789':
            num = 10*num + int(ch)
            pass
        else:
            if ch == '.':
                pass
            else:
                symbol_coords.append((r, c))
                if ch == '*':
                    gear_coords.append([r, c, 0, 0])  # last numbers are the two adjacent numbers
            if num != 0:
                nums_w_bounds.append((r, num, c-len(str(num)), c-1))
            num = 0
    if num != 0:
        nums_w_bounds.append((r, num, c-len(str(num)), c-1))

part_one_set = set([])
for r, c in symbol_coords:
    for i in range(len(nums_w_bounds)):
        num_w_bounds = nums_w_bounds[i]
        rbs, rbe, cbs, cbe = num_w_bounds[0] - 1, num_w_bounds[0] + 1, num_w_bounds[2] - 1, num_w_bounds[3] + 1
        if rbs <= r and r <= rbe and cbs <= c and c <= cbe:
            part_one_set |= set([i])

partnum_sum = 0
for i in reversed(sorted(part_one_set)):
    partnum_sum += nums_w_bounds[i][1]

# Part 2
for i in range(len(gear_coords)):
    r, c, _, _ = gear_coords[i]
    for num_w_bounds in nums_w_bounds:
        rbs, rbe, cbs, cbe = num_w_bounds[0] - 1, num_w_bounds[0] + 1, num_w_bounds[2] - 1, num_w_bounds[3] + 1
        if rbs <= r and r <= rbe and cbs <= c and c <= cbe:
            if gear_coords[i][2] == 0 and gear_coords[i][3] == 0:
                gear_coords[i][2] = num_w_bounds[1]
            elif gear_coords[i][3] == 0:
                gear_coords[i][3] = num_w_bounds[1]

gear_sum = 0
for _, _, n1, n2 in gear_coords:
    gear_sum += n1 * n2

print(f'Part 1: {partnum_sum}')
print(f'Part 2: {gear_sum}')

