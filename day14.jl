using DelimitedFiles

try
    global fh = open("input14.txt", "r")
catch
    global fh = open("sample14.txt", "r")
end

board = permutedims.(collect.(readdlm(fh, String)));
close(fh)
nr = length(board);
nc = length(board[1]);

for _ in 1:nr
	changes = false
	for c in 1:nc
		for r in 2:nr
			if board[r][c] == 'O' && board[r-1][c] == '.'
				board[r-1][c] = 'O'
				board[r][c] = '.'
				changes = true
			end
		end
	end

	if !changes
		break
	end
end

score = 0
for (row_num, row) in enumerate(board)
	global score += sum([ch == 'O' ? (nr - row_num + 1) : 0 for ch in row])
end

println("Part 1: ", score)
