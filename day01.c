#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	int result;
	FILE *fp;
	char *inputString = NULL;
	size_t len;

	int part = 1;

	while (part <= 2) {
		result = 0;

		fp = fopen("input01.txt", "r");
		if (fp == NULL && part == 1)  { fp = fopen("sample01.txt", "r"); }
		if (fp == NULL && part == 2)  { fp = fopen("sample01b.txt", "r"); }

		while ( getline(&inputString, &len, fp) != -1 ) {
			int digitL = -1;
			int digitR;
			int digitCnt = -1;
			int digitA = 0;
			char *c = &inputString[0];

			while (*c != '\0') {
				if (part == 1 && *c >= 0x30 && *c <= 0x39) {
					digitL = (digitL == -1) ? *c - 0x30 : digitL;
					digitR = *c - 0x30;
				}

				// By-hand implementation of a trie
				if (part == 2) {
					switch (*c) {
						case 'e':
							switch (digitCnt) {
								case 33:  digitCnt = 34; break;
								case 69:  digitCnt = 72; break;
								case 73:  digitCnt = 74; break;
								case 12:  digitCnt = 81; digitA = 1; break;
								case 34:  digitCnt = 81; digitA = 3; break;
								case 53:  digitCnt = 81; digitA = 5; break;
								case 93:  digitCnt = 81; digitA = 9; break;
								default:  digitCnt = 81;
							}
							break;
						case 'f':
							switch (digitCnt) {
								default:  digitCnt = 49;
							}
							break;
						case 'g':
							switch (digitCnt) {
								case 82:  digitCnt = 83; break;
								default:  digitCnt = -1;
							}
							break;
						case 'h':
							switch (digitCnt) {
								case 29:  digitCnt = 32; break;
								case 83:  digitCnt = 84; break;
								default:  digitCnt = -1;
							}
							break;
						case 'i':
							switch (digitCnt) {
								case 34:  digitCnt = 82; break;  // threight
								case 72:  digitCnt = 82; break;  // seight
								case 74:  digitCnt = 82; break;  // seveight
								case 12:  digitCnt = 92; break;  // onine
								case 93:  digitCnt = 92; break;  // ninine

								case 49:  digitCnt = 52; break;
								case 69:  digitCnt = 62; break;
								case 81:  digitCnt = 82; break;
								case 91:  digitCnt = 92; break;
								default:  digitCnt = -1;
							}
							break;
						case 'n':
							switch (digitCnt) {
								case 42:  digitCnt = 12; break;  // fone

								case 11:  digitCnt = 12; break;
								case 92:  digitCnt = 93; break;
								case 74:  digitCnt = 91; digitA = 7; break;
								default:  digitCnt = 91;
							}
							break;
						case 'o':
							switch (digitCnt) {
								case 49:  digitCnt = 42; break;
								case 22:  digitCnt = 11; digitA = 2; break;
								default:  digitCnt = 11;
							}
							break;
						case 'r':
							switch (digitCnt) {
								case 32:  digitCnt = 33; break;
								case 43:  digitCnt = -1; digitA = 4; break;
								default:  digitCnt = -1;
							}
							break;
						case 's':
							switch (digitCnt) {
								default:  digitCnt = 69;
							}
							break;
						case 't':
							switch (digitCnt) {
								case 84:  digitCnt = 29; digitA = 8; break;
								default:  digitCnt = 29;
							}
							break;
						case 'u':
							switch (digitCnt) {
								case 42:  digitCnt = 43; break;
								default:  digitCnt = -1;
							}
							break;
						case 'v':
							switch (digitCnt) {
								case 52:  digitCnt = 53; break;
								case 72:  digitCnt = 73; break;
								default:  digitCnt = -1;
							}
							break;
						case 'w':
							switch (digitCnt) {
								case 29:  digitCnt = 22; break;
								default:  digitCnt = -1;
							}
							break;
						case 'x':
							switch (digitCnt) {
								case 62:  digitCnt = -1; digitA = 6; break;
								default:  digitCnt = -1;
							}
							break;
						case '1':  digitCnt = -1; digitA = 1; break;
						case '2':  digitCnt = -1; digitA = 2; break;
						case '3':  digitCnt = -1; digitA = 3; break;
						case '4':  digitCnt = -1; digitA = 4; break;
						case '5':  digitCnt = -1; digitA = 5; break;
						case '6':  digitCnt = -1; digitA = 6; break;
						case '7':  digitCnt = -1; digitA = 7; break;
						case '8':  digitCnt = -1; digitA = 8; break;
						case '9':  digitCnt = -1; digitA = 9; break;
						default:
							digitCnt = -1;
					}

					if (digitA) {
						digitL = (digitL == -1) ? digitA : digitL;
						digitR = digitA;
						digitA = 0;
					}
				}

				c++;
			}

			result += 10*digitL + digitR;
		}

		printf("Result for Part %d: %d\n", part, result);

		fclose(fp);
		part++;
	}

	free(inputString);
}
