#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

int main() {
	vector<pair<long int, unsigned char>> seeds;  // value, current map
	string line;

	ifstream fs("input05.txt", ifstream::in);
	if (fs.fail()) {
		fs.clear();
		fs.open("sample05.txt", ifstream::in);
	}

	// Parse input seeds
	getline(fs, line);
	istringstream ss(line);
	ss >> line;  // Throwaway string part of input
	long int seed;
	while (ss.peek() != -1) {
		ss >> seed;
		seeds.push_back(make_pair(seed, 0));
	}

	// Move input seeds along mapping
	getline(fs, line);
	for (int mapping = 1; mapping <= 7; ++mapping) {
		getline(fs, line);
		long int dst, src, len;
		while ( getline(fs, line) ) {
			if (line.size() == 0)  { break; }
			istringstream ss(line);
			ss >> dst >> src >> len;  // Stream is easier for discarding \n and assigning to vars with >>
			for (auto& seedMap : seeds) {
				if (seedMap.second == mapping)  { continue; }  // Skip seeds if already moved
				if (seedMap.first >= src && seedMap.first < src + len) {
					seedMap.second = mapping;
					seedMap.first += (dst - src);
				}
			}
		}
	}

	sort(seeds.begin(), seeds.end()); 

	fs.close();

	cout << "Part 1: " << seeds.front().first << endl;

	return EXIT_SUCCESS;
}
