try:
    infile = open('input13.txt', 'r')
except:
    infile = open('sample13.txt', 'r')

puzzle_strings = infile.read().strip().split('\n\n')

make_reflect_set = lambda s: set(range(1, 1 + s))

score = 0
for puzzle_string in puzzle_strings:
    puzzle_2d = puzzle_string.split('\n')

    nr, nc = len(puzzle_2d), len(puzzle_2d[0])
    rs_cols = make_reflect_set(nc)
    rs_rows = make_reflect_set(nr)

    # For each row, check for a column reflection by subtracting mismatches from rs_cols
    for this_row in puzzle_2d:
        rs_cur = set()
        for c in range(1, nc):
            if all([a == b for a,b in zip(this_row[c-1::-1], this_row[c:])]):
                rs_cur |= set([c])
        rs_cols &= rs_cur
        if len(rs_cols) == 0:
            break

    # If a reflection is found, add to score
    if len(rs_cols) == 1:
        score += rs_cols.pop()
        continue

    # If rs_cols becomes empty, skip to the next segment: transpose matrix, do again.
    for this_col in zip(*puzzle_2d):
        rs_cur = set()
        for r in range(1, nr):
            if all([a == b for a,b in zip(this_col[r-1::-1], this_col[r:])]):
                rs_cur |= set([r])
        rs_rows &= rs_cur

    score += 100*rs_rows.pop()

print(f'Part 1: {score}')

