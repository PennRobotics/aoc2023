try:
    infile = open('input02.txt', 'r')
except:
    infile = open('sample02.txt', 'r')

total = 0
power_sum = 0
for line in infile:  # Ex -> "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n"
    good_game = True
    fewest = [0, 0, 0]  # Fewest number of R, G, B seen in each game (for part 2)

    game_id_str, rounds = line[:-1].split(': ')
    game_id = int(game_id_str.split(' ')[1])

    for rnd in rounds.split('; '):  # ["3 blue, 4 red", "1 red, 2 green", ...]
        cnt_and_col_list = rnd.split(', ')  # ["3 blue", "4 red"]
        for cnt_and_col in cnt_and_col_list:
            cnt_str, col = cnt_and_col.split(' ')  # ["3", "blue"]
            cnt = int(cnt_str)
            match col:
                case 'red':
                    good_game = False if cnt > 12 else good_game  # Part 1 game fails when not enough cubes exist
                    fewest[0] = cnt if cnt > fewest[0] else fewest[0]
                case 'green':
                    good_game = False if cnt > 13 else good_game
                    fewest[1] = cnt if cnt > fewest[1] else fewest[1]
                case 'blue':
                    good_game = False if cnt > 14 else good_game
                    fewest[2] = cnt if cnt > fewest[2] else fewest[2]

    total += game_id if good_game else 0
    power_sum += fewest[0] * fewest[1] * fewest[2]

print(f'Part 1: {total}')
print(f'Part 2: {power_sum}')

