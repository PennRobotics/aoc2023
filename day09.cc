#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

int main() {
	long int num;
	vector<long int> v;
	vector<long int> c;
	vector<long int> c2;
	long int result = 0;
	long int result2 = 0;

	ifstream fs("input09.txt", ifstream::in);
	if (fs.fail()) {
		fs.clear();
		fs.open("sample09.txt", ifstream::in);
	}

	while (fs.peek() != EOF) {
		// Fill a vector with one line of numbers
		v.clear();
		while (fs >> num) {
			v.push_back(num);
			if (fs.peek() == '\n')  { break; }
		}

		// Get each discrete derivative (I think the math people call this a difference)
		//   plus the constant removed on each iteration
		c.clear();
		c2.clear();
		while (any_of(v.begin(), v.end(), [](long int e){ return e != 0; })) {
			c.push_back(v.back());
			c2.push_back(v.front());
			adjacent_difference(v.begin(), v.end(), v.begin());
			v.erase(v.begin());
		}

		result += reduce(c.begin(), c.end());

		// Flip sign of every other row (This was a bit of a guess, since 1 + -2 + 1 = 0
		//   was the only way to resolve the second sample values.)
		for (int i = 0; i < c2.size(); ++i)  { c2[i] = (i % 2) ? -c2[i] : c2[i]; }
		result2 += reduce(c2.begin(), c2.end());
	}

	fs.close();

	cout << "Part 1: " << result << endl;
	cout << "Part 2: " << result2 << endl;

	return EXIT_SUCCESS;
}
